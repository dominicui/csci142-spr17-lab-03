package testcases;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import structures.LinkedList;

/**
 * test functionality of linkedList
 * add, remove, insert, index, get first, get last
 * 
 * @author dominic
 *
 */

public class LinkListTestCases 
{

	private LinkedList<String> myLinkedList;
	
	// set link list as "stetson"
	
	@Before
	public void setUp()
	{
		myLinkedList = new LinkedList<String>("n");
		myLinkedList.addFirst("o");	
		myLinkedList.addFirst("s");
		myLinkedList.addFirst("t");
		myLinkedList.addFirst("e");
		myLinkedList.addFirst("t");
		myLinkedList.addFirst("s");	
	}
	
	// test remove
	
	@Test
	public void testRemoveUnique() 
	{
		myLinkedList.remove("o");
		assertFalse("No 'o' in the LinkList", myLinkedList.contains("o"));
	}
	
	@Test
	public void testRemoveNull() 
	{
		assertTrue("Null removed", myLinkedList.remove(null));
	}
	
	@Test
	public void testRemove() 
	{
		myLinkedList.remove("b");
		assertFalse("'b' is not exist in link list", myLinkedList.contains("b"));
	}
	
	@Test
	public void testRemoveMulti() 
	{
		myLinkedList.remove("s");
		assertTrue("There exists 's' in the LinkList", myLinkedList.contains("s"));
	}

	@Test
	public void testRemoveMulti1() 
	{
		myLinkedList.remove("s");
		myLinkedList.remove("s");
		assertFalse("No 's' in the LinkList", myLinkedList.contains("s"));
	}

	@Test
	public void testRemoveMultiAdd() 
	{
		myLinkedList.remove("t");
		myLinkedList.remove("t");
		myLinkedList.addFirst("t");
		assertTrue("There exists 't' in the LinkList", myLinkedList.contains("t"));
	}
	
	@Test
	public void testRemoveMultiAddIndex() 
	{
		myLinkedList.remove("t");
		myLinkedList.remove("t");
		myLinkedList.addFirst("t");
		int index = myLinkedList.indexOf("t");
		assertTrue("There exists 't' in the LinkList", index == 1);
	}
	
	// test remove first
	
	@Test
	public void testRemoveFirst() 
	{
		myLinkedList.removeFirst();	
		assertTrue("The first data is 't'", myLinkedList.getFirst() == "t");
	}
	
	@Test
	public void testRemoveFirst1() 
	{
		myLinkedList.removeFirst();	
		myLinkedList.removeFirst();	
		assertTrue("The first data is 'e'", myLinkedList.getFirst() == "e");
		assertTrue("The size of link list is 5", myLinkedList.size() == 5 );
	}
	
	@Test
	public void testRemoveFirst2() 
	{
		myLinkedList.removeFirst();	
		myLinkedList.removeFirst();	
		myLinkedList.removeFirst();	
		myLinkedList.removeFirst();	
		myLinkedList.removeFirst();	
		myLinkedList.removeFirst();	
		myLinkedList.removeFirst();	
		
		assertTrue("The size of link list is 0", myLinkedList.size() == 0 );
	}
	
	// test remove last
	
	@Test
	public void testRemoveLast() 
	{
		myLinkedList.removeLast();	
		assertTrue("The last data is 'o'", myLinkedList.getLast() == "o");
	}
	
	@Test
	public void testRemoveLast1() 
	{
		myLinkedList.removeLast();	
		myLinkedList.removeLast();	
		assertTrue("The last data is 'e'", myLinkedList.getFirst() == "s");
		assertTrue("The size of link list is 5", myLinkedList.size() == 5 );
	}
	
	@Test
	public void testRemoveLast2() 
	{
		myLinkedList.removeLast();	
		myLinkedList.removeLast();	
		myLinkedList.removeLast();	
		myLinkedList.removeLast();	
		myLinkedList.removeLast();	
		myLinkedList.removeLast();	
		myLinkedList.removeLast();	
		
		assertTrue("The size of link list is 0", myLinkedList.size() == 0 );
	}
		
	// test add first
	
	@Test
	public void testAdd() 
	{
		myLinkedList.addFirst("y");		
		assertTrue("There exists 'y' in the LinkList", myLinkedList.contains("y"));
	}
	
	@Test
	public void testAddIndex() 
	{
		myLinkedList.addFirst("y");	
		int index = myLinkedList.indexOf("y");
		assertTrue("There exists 'y' at index 1 of LinkList", index == 1);
	}
	
	@Test
	public void testAddMulti() 
	{
		myLinkedList.addFirst("y");	
		myLinkedList.addFirst("z");		
		assertTrue("There exists 'y' & 'z' in the LinkList", myLinkedList.contains("y") && myLinkedList.contains("z"));
	}
	
	@Test
	public void testAddMultiIndex() 
	{
		myLinkedList.addFirst("y");	
		int indexY = myLinkedList.indexOf("y");
		assertTrue("There exists 'y' at index 1 of LinkList", indexY == 1);
		
		myLinkedList.addFirst("z");	
		int indexZ = myLinkedList.indexOf("z");
		assertTrue("There exists 'z' at index 1 of LinkList", indexZ == 1);
	}
	
	@Test
	public void testAddMultiIndex1() 
	{
		myLinkedList.addFirst("y");	
		myLinkedList.addFirst("z");	
		int indexY = myLinkedList.indexOf("y");
		assertTrue("There exists 'y' at index 2 of LinkList", indexY == 2);	
	}
		
	// test add last
	
	@Test
	public void testAddLast() 
	{
		myLinkedList.addLast("y");		
		assertTrue("There exists 'y' in the LinkList", myLinkedList.contains("y"));
	}
	
	@Test
	public void testAddLastIndex() 
	{
		myLinkedList.addLast("y");	
		int index = myLinkedList.indexOf("y");
		assertTrue("There exists 'y' at index 8 of LinkList", index == 8);
	}
	
	@Test
	public void testAddLastMulti() 
	{
		myLinkedList.addLast("y");	
		myLinkedList.addLast("z");		
		assertTrue("There exists 'y' & 'z' in the LinkList", myLinkedList.contains("y") && myLinkedList.contains("z"));
	}
	
	@Test
	public void testAddLastMultiIndex() 
	{
		myLinkedList.addLast("y");	
		int indexY = myLinkedList.indexOf("y");
		assertTrue("There exists 'y' at index 8 of LinkList", indexY == 8);
		
		myLinkedList.addLast("z");	
		int indexZ = myLinkedList.indexOf("z");
		assertTrue("There exists 'z' at index 9 of LinkList", indexZ == 9);
	}
	
	@Test
	public void testAddLastMultiIndex1() 
	{
		myLinkedList.addLast("y");	
		myLinkedList.addLast("z");	
		int indexY = myLinkedList.indexOf("y");
		assertTrue("There exists 'y' at index 8 of LinkList", indexY == 8);	
	}
	
    // test insert before
	
	@Test
	public void testInsertToFirst() 
	{
		myLinkedList.insertBefore("d", "s");	
		assertTrue("There exists 'd' in the LinkList", myLinkedList.contains("d"));
	}
	
	@Test
	public void testInsertToFirstIndex() 
	{
		myLinkedList.insertBefore("d", "s");	
		int index = myLinkedList.indexOf("d");
		assertTrue("There exists 'd' at index 1 of LinkList", index == 1);
	}
	
	@Test
	public void testInsert() 
	{
		myLinkedList.insertBefore("d", "e");	
		assertTrue("There exists 'd' in the LinkList", myLinkedList.contains("d"));
	}
	
	@Test
	public void testInsertIndex() 
	{
		myLinkedList.insertBefore("d", "e");
		int index = myLinkedList.indexOf("d");
		assertTrue("There exists 'd' at index 3 of LinkList", index == 3);
	}
	
	//test get first
	
	@Test
	public void testFirst()
	{
		String first = myLinkedList.getFirst();
		assertTrue("The first data of link list is 's'", first == "s");
	}
	
	@Test
	public void testFirst1()
	{
		myLinkedList.remove("s");
		myLinkedList.remove("e");
		myLinkedList.remove("o");
		String first = myLinkedList.getFirst();
		assertTrue("The first data of link list is 't'", first == "t");
	}
	
	@Test
	public void testSetFirst()
	{		
		myLinkedList.setFirst("a");
		String first = myLinkedList.getFirst();
		assertTrue("The first data of link list is 'a'", first == "a");
	}
	
	// test get last
	
	@Test
	public void testLast()
	{
		String last = myLinkedList.getLast();
		assertTrue("The last data of link list is 'n'", last == "n");
	}
	
	@Test
	public void testSetLast()
	{			
		myLinkedList.addLast("z");
		String last = myLinkedList.getLast();
		assertTrue("The last data of link list is 'z'", last == "z");
	}
	
}
